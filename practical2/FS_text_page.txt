******************************************************************************
index.html
##############################################################################
 article

 Full Stack Specialization

Students learn to translate user requirements into the overall software architecture and implement the new solution as web or mobile applications,
using cutting-edge frameworks, state-of-the-art toolsets on cloud and enterprise technologies.

Full-Stack Professional

As a Full-Stack Professional, he or she is expected to work on both Front End & Back End Development, and applying DevOps skills to deploy workloads through Continuous Integration/Continuous Deployment (CI/CD) pipeline.
He or she should have a genuine interest in all software technologies.

Program Structure

The program is designed as a four-year undergraduate degree.
The first year is broadly a foundation year comprising modules that will be built upon it in the subsequent years.
Each semester will have five theory modules or projects that will deliver practical sessions to promote inquiry-based learning.
##############################################################################
footer

Full Stack Developers
Gyalpozhing College Of Information Technology
Mongar: BHUTAN

******************************************************************************
Reviews.html
##############################################################################

Generally a good work place. It really depends on what team you work on.
Management is too busy to pay attention to your career advancement. 
Sometimes you feel like you are lost in an ocean
Feedback from
Kinley Tshering

I had a great time in GCIT as a Developer. 
Management was great and my experience was tough work, stressful, but overall a great time. 
People are mostly good. Working with professionals to deliver the best service possible. 
Part of a culture that believes that anything is possible.

Acknowledge by
Namgay Dema
            
            
